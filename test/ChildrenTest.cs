using System;
using System.Collections.Generic;
using Leopotam.Ecs;
using NUnit.Framework;

namespace Kk.Kids4LeoEcs
{
    [TestFixture]
    public class ChildrenTest
    {
        [Serializable]
        public struct Name
        {
            public string value;

            public Name(string value)
            {
                this.value = value;
            }
        }

        [Test]
        public void CanAdd()
        {
            EcsWorld world = new EcsWorld();

            EcsEntity root = world.NewEntity().Replace(new Name("A"));
            root.SpawnKid()
                .Replace(new Name("B"));

            List<string> names = new List<string>();
            foreach (EcsEntity kid in root.Kids())
            {
                names.Add(kid.Get<Name>().value);
            }

            Assert.AreEqual(new List<string> {"B"}, names);
        }

        [Test]
        public void Iterated()
        {
            EcsWorld world = new EcsWorld();

            EcsEntity root = world.NewEntity();
            root.SpawnKid()
                .Replace(new Name("A"));
            root.SpawnKid()
                .Replace(new Name("B"));
            root.SpawnKid()
                .Replace(new Name("C"));

            List<string> names = new List<string>();
            foreach (EcsEntity kid in root.Kids())
            {
                names.Add(kid.Get<Name>().value);
            }

            Assert.AreEqual(new List<string> {"A", "B", "C"}, names);
        }

        [Test]
        public void IteratedViaWith()
        {
            EcsWorld world = new EcsWorld();

            EcsEntity root = world.NewEntity();
            root.SpawnKid()
                .Replace(new Name("A"));
            root.SpawnKid()
                .Replace(new Name("B"));
            root.SpawnKid()
                .Replace(new Name("C"));

            List<string> names = new List<string>();
            foreach (Kid<Name> kid in root.Kids().With<Name>())
            {
                names.Add(kid.Get1().value);
            }

            Assert.AreEqual(new List<string> {"A", "B", "C"}, names);
        }

        [Test]
        public void AutoRemoveDestroyed()
        {
            EcsWorld world = new EcsWorld();
            EcsEntity root = world.NewEntity()
                .Replace(new Name("A"));

            EcsEntity b = root.SpawnKid()
                .Replace(new Name("B"));

            root.SpawnKid().Replace(new Name("C"));

            b.Destroy();

            List<string> names = new List<string>();
            foreach (EcsEntity kid in root.Kids())
            {
                names.Add(kid.Get<Name>().value);
            }

            Assert.AreEqual(new List<string> {"C"}, names);
        }

        [Test]
        public void AutoRemoveDestroyed2()
        {
            EcsWorld world = new EcsWorld();
            EcsEntity root = world.NewEntity();

            root.SpawnKid()
                .Replace(new Name("A"));
            root.SpawnKid()
                .Replace(new Name("B"));
            root.SpawnKid()
                .Replace(new Name("C"));

            foreach (Kid<Name> kid in root.Kids().With<Name>())
            {
                kid.GetEntity().Destroy();
            }

            List<string> names = new List<string>();
            foreach (EcsEntity child in root.Kids())
            {
                names.Add(child.Get<Name>().value);
            }

            Assert.AreEqual(new List<string> { }, names);
        }

        public struct X { }

        [Test]
        public void SkipsComponentIfFiltered()
        {
            EcsWorld world = new EcsWorld();
            EcsEntity root = world.NewEntity()
                .Replace(new Name("A"));

            root.SpawnKid()
                .Replace(new Name("B"));
            root.SpawnKid()
                .Replace(new Name("C"))
                .Replace(new X());
            root.SpawnKid()
                .Replace(new Name("D"))
                .Replace(new X());
            root.SpawnKid()
                .Replace(new Name("E"));

            List<string> names = new List<string>();
            foreach (Kid<Name, X> kid in root.Kids().With<Name, X>())
            {
                names.Add(kid.Get1().value);
            }

            Assert.AreEqual(new List<string> {"C", "D"}, names);
        }

        [Test]
        public void SkipsGlobalComponents()
        {
            EcsWorld world = new EcsWorld();
            EcsEntity root = world.NewEntity()
                .Replace(new Name("A"))
                .Replace(new X());

            root.SpawnKid()
                .Replace(new Name("B"))
                .Replace(new X());

            List<string> names = new List<string>();
            foreach (Kid<Name, X> kid in root.Kids().With<Name, X>())
            {
                names.Add(kid.Get1().value);
            }

            Assert.AreEqual(new List<string> {"B"}, names);
        }

        struct Key
        {
            public int value;

            public Key(int value)
            {
                this.value = value;
            }
        }

        struct Value
        {
            public int value;

            public Value(int value)
            {
                this.value = value;
            }
        }

        [Test]
        public void TestFilterBy()
        {
            EcsWorld world = new EcsWorld();
            EcsEntity root = world.NewEntity();

            root.SpawnKid().Replace(new Key(2)).Replace(new Value(102));
            root.SpawnKid().Replace(new Key(3)).Replace(new Value(103));

            List<int> values = new List<int>();
            foreach (Kid<Key, Value> kid in root.Kids().With<Key, Value>().Search1(new Key(2)))
            {
                values.Add(kid.Get2().value);
            }

            Assert.AreEqual(new List<int> {102}, values);
        }

        [Test]
        public void TestFilterByMultiple()
        {
            EcsWorld world = new EcsWorld();
            EcsEntity root = world.NewEntity();

            root.SpawnKid().Replace(new Key(2)).Replace(new Value(102));
            root.SpawnKid().Replace(new Key(2)).Replace(new Value(102));
            root.SpawnKid().Replace(new Key(3)).Replace(new Value(103));

            List<int> values = new List<int>();
            foreach (Kid<Key, Value> kid in root.Kids().With<Key, Value>().Search1(new Key(2)))
            {
                values.Add(kid.Get2().value);
            }

            Assert.AreEqual(new List<int> {102, 102}, values);
        }

        [Test]
        public void ParentAvailable()
        {
            EcsWorld world = new EcsWorld();
            EcsEntity root = world.NewEntity();

            EcsEntity child = root.SpawnKid();
            
            Assert.AreEqual(root, child.KidParent());
        }

        [Test]
        public void ChildrenDestroyedByCascade()
        {
            EcsWorld world = new EcsWorld();
            
            EcsFilter<Name> filter = (EcsFilter<Name>) world.GetFilter(typeof(EcsFilter<Name>));
            
            EcsEntity root = world.NewEntity().Replace(new Name("root"));
            EcsEntity root2 = world.NewEntity().Replace(new Name("root2"));

            root.SpawnKid().Replace(new Name("A"));
            root.SpawnKid().Replace(new Name("B"));
            root2.SpawnKid().Replace(new Name("A2"));
            
            root.Destroy();

            List<string> names = new List<string>();
            foreach (int i in filter)
            {
                names.Add(filter.Get1(i).value);
            }
            names.Sort();
            Assert.AreEqual("A2,root2", string.Join(",", names));
        }

        struct NotPresent {}
        
        [Test]
        public void QueueIsNotEmptyBug()
        {
            EcsWorld world = new EcsWorld();
            EcsEntity root = world.NewEntity();
            root.SpawnKid().Replace(new Name("A"));

            // locking and not unlocking
            root.Kids().With<NotPresent>().SingleOrNull();
            
            EcsEntity b = root.SpawnKid();
            // added to queue
            b.Destroy();
            
            // crashes with "_queue is not empty"
            root.Destroy();
        }

        [Test]
        public void NotSingleBug()
        {
            EcsWorld world = new EcsWorld();
            EcsEntity root = world.NewEntity();

            root.SpawnKid()
                .Replace(new Key(12));
            foreach (Kid<Key> kid in root.Kids().With<Key>())
            {
                kid.GetEntity().Destroy();
            }

            root.SpawnKid()
                .Replace(new Key(12));
            root.Kids().With<Key>()
                .Search1(new Key(12))
                .Single();
        }
    }
}