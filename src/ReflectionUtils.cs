using System;
using System.Diagnostics;
using System.Linq.Expressions;
using System.Reflection;

namespace Kk.Kids4LeoEcs
{
    internal static class ReflectionUtils
    {
        public static Func<TInstance, TField> CreateFieldGetter<TInstance, TField>(string fieldName)
        {
            FieldInfo propertyInfo = typeof(TInstance).GetField(
                fieldName,
                BindingFlags.NonPublic | BindingFlags.Instance
            );
            Debug.Assert(propertyInfo != null, nameof(propertyInfo) + " != null");
            ParameterExpression parameter = Expression.Parameter(typeof(TInstance));
            MemberExpression body = Expression.Field(parameter, propertyInfo);
            return Expression.Lambda<Func<TInstance, TField>>(body, parameter).Compile();
        }
    }
}