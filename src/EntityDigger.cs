using System;
using System.Collections.Generic;
using Leopotam.Ecs;

namespace Kk.Kids4LeoEcs
{
    internal class EntityDigger : IEcsWorldDebugListener
    {
        private readonly EcsGrowList<EcsEntity> _list;
        private readonly EcsEntity _self;
        private readonly Queue<EcsEntity> _queue;
        private int _lockCount;
        private bool _destroying;

        public EntityDigger(EcsGrowList<EcsEntity> list, EcsEntity self)
        {
            _list = list;
            _self = self;
            _queue = new Queue<EcsEntity>(list.Items.Length);
        }

        public void OnEntityCreated(EcsEntity entity) { }

        public void OnEntityDestroyed(EcsEntity entity)
        {
            if (_destroying)
            {
                return;
            }
            if (entity == _self)
            {
                _destroying = true;
                for (var i = 0; i < _list.Count; i++)
                {
                    if (_list.Items[i].IsAlive())
                    {
                        _list.Items[i].Destroy();
                    }
                }

                if (_queue.Count != 0)
                {
                    throw new Exception("_queue is not empty");
                }
            }
            else
            {
                if (_lockCount > 0)
                {
                    _queue.Enqueue(entity);
                }
                else
                {
                    Delete(entity);
                    ProcessDeleteQueue();
                }
            }
        }

        private void Delete(in EcsEntity entity)
        {
            foreach (EcsEntity kid in entity.Kids())
            {
                _queue.Enqueue(kid);
            }
            for (int i = _list.Count - 1; i >= 0; i--)
            {
                if (_list.Items[i] != entity) continue;
                _list.Count--;
                _list.Items[i] = _list.Items[_list.Count];
            }
        }

        public void OnFilterCreated(EcsFilter filter) { }

        public void OnComponentListChanged(EcsEntity entity) { }

        public void OnWorldDestroyed(EcsWorld world) { }

        public void Lock()
        {
            if (_lockCount > 0)
            {
                // nested loops over the same collection of entities also doesn't make sense, so most likely that's an enumerators leak
                throw new Exception("nested loops not supported");
            }

            _lockCount++;
        }

        public void Unlock()
        {
            _lockCount--;
            if (_lockCount <= 0)
            {
                ProcessDeleteQueue();
            }
        }

        private void ProcessDeleteQueue()
        {
            while (_queue.Count > 0)
            {
                EcsEntity entity = _queue.Dequeue();
                Delete(entity);
            }
        }

        public override string ToString()
        {
            return $"{nameof(_queue)}: {_queue.Count}, {nameof(_lockCount)}: {_lockCount}, {nameof(_destroying)}: {_destroying}";
        }
    }
}