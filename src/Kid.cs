using Leopotam.Ecs;

namespace Kk.Kids4LeoEcs
{
    public struct Kid<T> where T : struct
    {
        private Kids.Enumerator _list;

        public Kid(in Kids.Enumerator list)
        {
            _list = list;
        }

        public ref EcsEntity GetEntity() => ref _list.CurrentRef;
        public ref T Get1() => ref _list.CurrentRef.Get<T>();
        
        public ref TX Req<TX>() where TX : struct
        {
            return ref _list.CurrentRef.GetRequired<TX>();
        }
    }

    public struct Kid<T1, T2> where T1 : struct where T2 : struct
    {
        private Kids.Enumerator _list;

        public Kid(in Kids.Enumerator list)
        {
            _list = list;
        }

        public ref EcsEntity GetEntity() => ref _list.CurrentRef;
        public ref T1 Get1() => ref _list.CurrentRef.Get<T1>();
        public ref T2 Get2() => ref _list.CurrentRef.Get<T2>();
        
        public ref TX Req<TX>() where TX : struct
        {
            return ref _list.CurrentRef.GetRequired<TX>();
        }
    }
}