using System;
using System.Collections;
using System.Collections.Generic;
using Leopotam.Ecs;

namespace Kk.Kids4LeoEcs
{
    public struct SelectedKids<T> : IEnumerable<Kid<T>>
        where T : struct
    {
        private Kids _list;
        private T? _match;

        public SelectedKids(Kids list, T? match)
        {
            _list = list;
            _match = match;
        }

        public Kid<T> Single()
        {
            using (Enumerator enumerator = GetEnumerator())
            {
                if (enumerator.MoveNext())
                {
                    Kid<T> single = enumerator.Current;
                    if (enumerator.MoveNext())
                    {
                        throw new Exception("not single");
                    }

                    return single;
                }
            }

            throw new Exception("not found");
        }

        public Kid<T>? SingleOrNull()
        {
            using (Enumerator enumerator = GetEnumerator())
            {
                if (enumerator.MoveNext())
                {
                    Kid<T> single = enumerator.Current;
                    if (enumerator.MoveNext())
                    {
                        throw new Exception("not single");
                    }

                    return single;
                }
            }

            return null;
        }

        public SelectedKids<T> Search1(T? match) => new SelectedKids<T>(_list, match);
        public Enumerator GetEnumerator() => new Enumerator(_list.GetEnumerator(), _match);
        IEnumerator<Kid<T>> IEnumerable<Kid<T>>.GetEnumerator() => GetEnumerator();
        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();

        public struct Enumerator : IEnumerator<Kid<T>>
        {
            private Kids.Enumerator _list;
            private T? _match;

            public Enumerator(Kids.Enumerator list, T? match)
            {
                _list = list;
                _match = match;
            }

            public Kid<T> Current => new Kid<T>(_list);
            public ref T Get1() => ref _list.CurrentRef.Get<T>();
            public ref EcsEntity GetEntity() => ref _list.CurrentRef;

            public bool MoveNext()
            {
                while (true)
                {
                    if (!_list.MoveNext())
                    {
                        return false;
                    }

                    ref EcsEntity entity = ref _list.CurrentRef;
                    if (!entity.IsAlive()) continue;

                    if (!entity.Has<T>()) continue;

                    if (_match.HasValue && !EqualityComparer<T>.Default.Equals(_match.Value, entity.Get<T>())) continue;

                    return true;
                }
            }

            public void Reset() => _list.Reset();
            object IEnumerator.Current => Current;
            public void Dispose() => _list.Dispose();
        }
    }

    public struct SelectedKids<T1, T2> : IEnumerable<Kid<T1, T2>>
        where T2 : struct
        where T1 : struct
    {
        private Kids _list;
        private T1? _match1;
        private T2? _match2;

        public SelectedKids(Kids list, T1? match1, T2? match2)
        {
            _list = list;
            _match1 = match1;
            _match2 = match2;
        }

        public Kid<T1, T2> Single()
        {
            using (Enumerator enumerator = GetEnumerator())
            {
                if (enumerator.MoveNext())
                {
                    Kid<T1, T2> single = enumerator.Current;
                    if (enumerator.MoveNext())
                    {
                        throw new Exception("not single");
                    }

                    return single;
                }
            }

            throw new Exception("not found");
        }

        public Kid<T1, T2>? SingleOrNull()
        {
            using (Enumerator enumerator = GetEnumerator())
            {
                if (enumerator.MoveNext())
                {
                    Kid<T1, T2> single = enumerator.Current;
                    if (enumerator.MoveNext())
                    {
                        throw new Exception("not single");
                    }

                    return single;
                }
            }

            return null;
        }

        public SelectedKids<T1, T2> Search1(T1? match1) => new SelectedKids<T1, T2>(_list, match1, _match2);
        public SelectedKids<T1, T2> Search2(T2? match2) => new SelectedKids<T1, T2>(_list, _match1, match2);
        public Enumerator GetEnumerator() => new Enumerator(_list.GetEnumerator(), _match1, _match2);
        IEnumerator<Kid<T1, T2>> IEnumerable<Kid<T1, T2>>.GetEnumerator() => GetEnumerator();
        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();

        public struct Enumerator : IEnumerator<Kid<T1, T2>>
        {
            private Kids.Enumerator _list;
            private T1? _match1;
            private T2? _match2;

            public Enumerator(Kids.Enumerator list, T1? match1, T2? match2)
            {
                _list = list;
                _match1 = match1;
                _match2 = match2;
            }

            public Kid<T1, T2> Current => new Kid<T1, T2>(_list);
            public ref T1 Get1() => ref _list.CurrentRef.Get<T1>();
            public ref T2 Get2() => ref _list.CurrentRef.Get<T2>();
            public ref EcsEntity GetEntity() => ref _list.CurrentRef;

            public bool MoveNext()
            {
                while (true)
                {
                    if (!_list.MoveNext())
                    {
                        return false;
                    }

                    ref EcsEntity entity = ref _list.CurrentRef;
                    if (!entity.IsAlive()) continue;

                    if (!entity.Has<T1>()) continue;
                    if (!entity.Has<T2>()) continue;

                    if (_match1.HasValue && !EqualityComparer<T1>.Default.Equals(_match1.Value, entity.Get<T1>())) continue;
                    if (_match2.HasValue && !EqualityComparer<T2>.Default.Equals(_match2.Value, entity.Get<T2>())) continue;

                    return true;
                }
            }

            public void Reset() => _list.Reset();
            object IEnumerator.Current => Current;
            public void Dispose() => _list.Dispose();
        }
    }
}