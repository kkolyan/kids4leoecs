using System;
using System.Collections;
using System.Collections.Generic;
using Leopotam.Ecs;

namespace Kk.Kids4LeoEcs
{
    [Serializable]
    internal struct KidsComponent : IEnumerable<EcsEntity>
    {
        private const int InitialCapacity = 8;

        private EcsGrowList<EcsEntity> _list;
        private EntityDigger _digger;

        public Kids Kids()
        {
            return new Kids(_list ?? Kids4LeoEcsUtils.EmptyList, _digger);
        }

        public void Add(EcsEntity self, EcsEntity entity)
        {
            if (_list == null)
            {
                _list = new EcsGrowList<EcsEntity>(InitialCapacity);
                _digger = new EntityDigger(_list, self);
                entity.GetOwner().AddDebugListener(_digger);
            }

            _list.Add(entity);
        }

        public override string ToString()
        {
            return $"items: {_list?.Count}, digger: {_digger}";
        }

        #region Enumerable

        // it's enumerable for serializer and pretty watches in debugger

        public Kids.Enumerator GetEnumerator() => Kids().GetEnumerator();

        IEnumerator<EcsEntity> IEnumerable<EcsEntity>.GetEnumerator() => GetEnumerator();

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();

        #endregion
    }
}