using System.Collections;
using System.Collections.Generic;
using Leopotam.Ecs;

namespace Kk.Kids4LeoEcs
{
    public struct Kids : IEnumerable<EcsEntity>
    {
        private readonly EcsGrowList<EcsEntity> _list;
        private readonly EntityDigger _entityDigger;

        internal Kids(EcsGrowList<EcsEntity> list, EntityDigger entityDigger)
        {
            _list = list;
            _entityDigger = entityDigger;
        }
        
        public SelectedKids<T> With<T>() where T : struct => new SelectedKids<T>(this, null);

        public SelectedKids<T1, T2> With<T1, T2>()
            where T1 : struct
            where T2 : struct
        {
            return new SelectedKids<T1, T2>(this, null, null);
        }

        public Enumerator GetEnumerator() => new Enumerator(_list, _entityDigger);
        
        IEnumerator<EcsEntity> IEnumerable<EcsEntity>.GetEnumerator() => GetEnumerator();
        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
        
        public struct Enumerator: IEnumerator<EcsEntity>
        {
            private int _index;
            private readonly EcsGrowList<EcsEntity> _list;
            private readonly EntityDigger _entityDigger;
            
            internal Enumerator(EcsGrowList<EcsEntity> list, EntityDigger entityDigger)
            {
                _list = list;
                _index = -1;
                _entityDigger = entityDigger;
                _entityDigger?.Lock();
            }

            public ref EcsEntity CurrentRef => ref _list.Items[_index];

            public EcsEntity Current => _list.Items[_index];

            public bool MoveNext()
            {
                while (true)
                {
                    _index++;
                    if (_index >= _list.Count)
                    {
                        return false;
                    }

                    if (!_list.Items[_index].IsAlive()) continue;

                    return true;
                }
            }

            public void Reset()
            {
                _index = -1;
            }

            object IEnumerator.Current => Current;

            public void Dispose()
            {
                _entityDigger?.Unlock();
            }
        }
    }
}