using System;
using Leopotam.Ecs;

namespace Kk.Kids4LeoEcs
{
    public static class Kids4LeoEcsUtils
    {
        
        internal static readonly EcsGrowList<EcsEntity> EmptyList = new EcsGrowList<EcsEntity>(1);
        
        public static EcsEntity SpawnKid(in this EcsEntity parent)
        {
            EcsWorld world = OwnerGetter(parent);
            EcsEntity child = world.NewEntity();
            parent.Get<KidsComponent>().Add(parent, child);
            child.Replace(new KidParentComponent(parent));
            return child;
        }

        public static EcsEntity KidParent(this EcsEntity entity)
        {
            if (!entity.Has<KidParentComponent>())
            {
                return EcsEntity.Null;
            }

            return entity.Get<KidParentComponent>().value;
        }

        public static Kids Kids(this EcsEntity entity)
        {
            if (entity.IsNull() || !entity.IsAlive() || !entity.Has<KidsComponent>())
            {
                return new Kids(EmptyList, null);
            }
            return entity.Get<KidsComponent>().Kids();
        }

        private static readonly Func<EcsEntity, EcsWorld> OwnerGetter =
            ReflectionUtils.CreateFieldGetter<EcsEntity, EcsWorld>("Owner");

        internal static EcsWorld GetOwner(this EcsEntity entity)
        {
            return OwnerGetter(entity);
        }
        
        internal static ref T GetRequired<T>(in this EcsEntity entity)
            where T : struct
        {
            if (!entity.IsAlive())
            {
                throw new Exception($"cannot access destroyed entity: {entity}");
            }

            if (!entity.Has<T>())
            {
                throw new Exception($"component not found: {typeof(T)} on entity {entity}");
            }

            return ref entity.Get<T>();
        }
    }
}