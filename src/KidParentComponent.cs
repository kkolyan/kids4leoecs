using System;
using Leopotam.Ecs;

namespace Kk.Kids4LeoEcs
{
    [Serializable]
    internal struct KidParentComponent
    {
        public EcsEntity value;

        public KidParentComponent(EcsEntity value)
        {
            this.value = value;
        }
    }
}