# About

Draft of extension for [LeoEcs](https://github.com/Leopotam/ecs) framework, that eases fine-chopped ECS applications.

# Features

* attaching "children" entities to any entity, working with them both by querying them from parent and querying by 
  EcsFilters like regular entities. 
* querying "children" entities in associative way, using one of components as a key.

# Examples

[Usage examples in test suite](test/ChildrenTest.cs)

# Limitations

* requires DEBUG compilation symbol
* this project is just a preview of idea. It's very far from being efficient and production ready.
